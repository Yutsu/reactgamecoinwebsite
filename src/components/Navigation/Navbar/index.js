import React, {useState, useEffect} from 'react';
import {FaBars} from 'react-icons/fa';
import {toggleHome} from '../../Global/ReactScroll';
import {IconContext} from 'react-icons/lib';
import {
    Nav, NavbarContainer,
    NavLogo, MobileIcon,
    NavMenu, NavItem,
    NavLinks, NavBtn,
    NavBtnLink
} from './styles.js';

const Navbar = ({toggle}) => {

    const [scrollNav, setScrollNav] = useState(false);
    const changeNav = () => {
        (window.scrollY >= 100) ? setScrollNav(true) : setScrollNav(false);
    }

    useEffect(() => {
        window.addEventListener('scroll', changeNav)
    }, []);

    return (
        <IconContext.Provider value ={{color: '#fff'}}>
            <Nav scrollNav={scrollNav}>
                <NavbarContainer>
                    <NavLogo to="/" onClick={toggleHome}> GameCoin </NavLogo>
                    <NavMenu>
                        <NavItem>
                            <NavLinks to="about"
                                smooth={true}
                                duration={500}
                                spy={true}
                                exact="true"
                                offset={-49}
                                activeClass="active"
                            >
                                About
                            </NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="discover"
                                smooth={true}
                                duration={500}
                                spy={true}
                                exact="true"
                                offset={-49}
                            >
                                Discover
                            </NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="services"
                                smooth={true}
                                duration={500}
                                spy={true}
                                exact="true"
                                offset={-48}
                            >
                                Services
                            </NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="signup"
                                smooth={true}
                                duration={500}
                                spy={true}
                                exact="true"
                                offset={-47}
                            >
                                Sign up
                            </NavLinks>
                        </NavItem>
                    </NavMenu>
                    <NavBtn>
                        <NavBtnLink to="/signin">Sign in</NavBtnLink>
                    </NavBtn>
                    <MobileIcon onClick={toggle}>
                        <FaBars />
                    </MobileIcon>
                </NavbarContainer>
            </Nav>
        </IconContext.Provider>
    );
};

export default Navbar;