import {animateScroll as scroll} from 'react-scroll';

export const toggleHome = () => {
    return scroll.scrollToTop();
}