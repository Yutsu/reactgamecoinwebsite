import React from 'react';
import { 
    Container,
    Form,
    FormWrap,
    H1,
    Icon,
    FormContent,
    FormLabel,
    FormInput,
    FormButton,
    Text
} from './styles.js';

const SigninSection = () => {
    return (
        <div>
             <Container>
                <FormWrap>
                    <Icon to="/">Gamecoin</Icon>
                    <FormContent>
                        <Form action="#">
                            <H1>Sign in to your account</H1>
                            <FormLabel htmlFor="for">Email</FormLabel> 
                            <FormInput type='email' required />
                            <FormLabel htmlFor="for">Password</FormLabel>
                            <FormInput type='password' required />
                            <FormButton type="submit">Continue</FormButton>
                            <Text>Forgot password</Text>
                        </Form>
                    </FormContent>
                </FormWrap>
            </Container> 
            <h1>hello</h1>
        </div>
    );
};

export default SigninSection;