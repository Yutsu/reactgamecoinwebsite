import imgInfo1 from '../../../assets/images/gameStopStronk.jpg';
import imgInfo2 from '../../../assets/images/info.png';
import imgInfo3 from '../../../assets/images/benef.jpg';

export const HomeObjectOne = {
    id: 'about',
    lightBg: true,
    lightText: false,
    lightTextDesc: true,
    topLine: 'Premium stock',
    headline: 'Unlimited transation with zero fees',
    description: `In partnership with WallStreetBoopboop, get access to our exclusive app that allows you
        to send unlimited transactions without getting charged any fees and you will stronk hard`,
    buttonLabel: 'Get started',
    imgStart: false,
    img: imgInfo1,
    alt: 'about',
    dark: false,
    primary: false,
    darkText: true
};

export const HomeObjectTwo = {
    id: 'discover',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Unlimited access',
    headline: 'Login to your account at any time',
    description: `We have you covered no matter where you are located. All
        your need is to have Internet connection and a phone or computer.`,
    buttonLabel: 'Learn more',
    imgStart: true,
    img: imgInfo2,
    alt: 'discover',
    dark: true,
    primary: true,
    darkText: false
};

export const HomeObjectThree = {
    id: 'signup',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Join our Team',
    headline: 'Creating an accout is extremely easy',
    description: `Get everything set up and ready in under 10 minutes. All
        you need to do is add your information and you are ready to go!`,
    buttonLabel: 'Start now',
    imgStart: false,
    img: imgInfo3,
    alt: 'signup',
    dark: true,
    primary: true,
    darkText: false
};