import React from 'react';
import icon1 from '../../../assets/images/stronk.jpg';
import icon2 from '../../../assets/images/gain.jpg';
import icon3 from '../../../assets/images/money.jpg';

import {
    ServicesContainer,
    H1,
    ServicesWrapper,
    ServicesCard,
    ServicesIcon,
    H2,
    P
} from './styles'

const ServicesSection = () => {
    return (
        <ServicesContainer id="services">
            <H1>Our services</H1>
            <ServicesWrapper>
                <ServicesCard>
                    <ServicesIcon src={icon1}/>
                    <H2>Reduce expenses</H2>
                    <P>We help reduce your fess and increase your overall revenue.</P>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon2}/>
                    <H2>Virtual Offices</H2>
                    <P>You can access our platform online anywhere in the world.</P>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon3}/>
                    <H2>Premium Benefits</H2>
                    <P>Unlock our special membership card that returns 5% cash back.</P>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
    );
};

export default ServicesSection;