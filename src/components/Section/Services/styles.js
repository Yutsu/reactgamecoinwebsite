import styled from 'styled-components';

export const ServicesContainer = styled.div `
    height: 115vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: #f9f9f9;


    @media screen and (max-width: 770px){
        height: 1300px;
    }

    /*@media screen and (max-width: 480px){
        height: 1300px;
    } */
`;

export const ServicesWrapper = styled.div `
    max-width: 1000px;
    margin: 0 auto;
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 16px;
    align-items: center;
    padding: 0 50px;

    @media screen and (max-width: 1000px){
        grid-template-columns: repeat(2, 1fr);
    }

    @media screen and (max-width: 770px){
        grid-template-columns: 1fr;
    }
`;

export const ServicesCard = styled.div `
    background: #f9f9f9;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    border-radius: 10px;
    max-height: 340px;
    padding: 30px;
    box-shadow: 2px 1px 7px rgba(0, 0, 0, 0.2);
    transition: all 0.2s ease-in-out;

    &:hover {
        transform: scale(1.02);
        transition: all 0.2s ease-in-out;
        cursor: pointer;
    }
`;

export const ServicesIcon = styled.img `
    width: 200px;
    height: 160px;
    margin-bottom: 10px;
    object-fit: cover;
`;

export const H1 = styled.h1 `
    font-size: 2.5rem;
    color: #010606;
    margin-bottom: 64px;

    @media screen and (max-width: 480px){
        font-size: 2rem;
    }
`;

export const H2 = styled.h2 `
    font-size: 1rem;
    margin-bottom: 10px;
`;

export const P = styled.p `
    font-size: 1rem;
    text-align: center;
`;
