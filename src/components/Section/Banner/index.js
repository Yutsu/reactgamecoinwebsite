import React, {useState} from 'react';
import video from '../../../assets/videos/video.mp4';
import {Button} from '../../Global/ButtonStyle';
import {
    BannerContainer,
    BannerBg,
    VideoBg,
    BannerContent,
    H1,
    P,
    BannerBtnWrapper,
    ArrowForward,
    ArrowRight
} from './styles';

const BannerSection = () => {
    const [hover, setHover] = useState(false);
    const onHover = () => {
        setHover(!hover)
    };

    return (
        <BannerContainer id="home">
            <BannerBg>
                <VideoBg playsInline autoPlay loop muted src={video} type="video/mp4" />
            </BannerBg>
            <BannerContent>
                <H1>Invest Gamestop stock with WSBoopboop</H1>
                <P>Sign up for a new account today and receive $250 from Melvin Capital </P>
                <BannerBtnWrapper>
                    <Button
                        to="signup"
                        onMouseEnter={onHover}
                        onMouseLeave={onHover}
                        primary='true'
                        dark='true'
                        smooth={true}
                        duration={500}
                        spy={true}
                        exact="true"
                        offset={0}
                        activeClass="active"
                    >
                        Get started {hover ? <ArrowForward /> : <ArrowRight/>}
                    </Button>
                </BannerBtnWrapper>
            </BannerContent>
        </BannerContainer>
    );
};

export default BannerSection;