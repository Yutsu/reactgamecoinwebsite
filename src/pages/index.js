import React, { useState } from 'react';
import Sidebar from '../components/Navigation/Sidebar';
import Navbar from '../components/Navigation/Navbar';
import HeroSection from '../components/Section/Banner';
import InfoSection from '../components/Section/Info';
import { HomeObjectOne, HomeObjectTwo, HomeObjectThree } from '../components/Section/Info/Data';
import ServicesSection from '../components/Section/Services';
import FooterSection from '../components/Section/Footer';

const Home = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => {
        setIsOpen(!isOpen);
    }

    return (
        <>
            <Navbar toggle={toggle}/>
            <Sidebar isOpen={isOpen} toggle={toggle} />
            <HeroSection />
            <InfoSection {...HomeObjectOne}/>
            <InfoSection {...HomeObjectTwo}/>
            <ServicesSection />
            <InfoSection {...HomeObjectThree}/>
            <FooterSection />
        </>
    );
};

export default Home;