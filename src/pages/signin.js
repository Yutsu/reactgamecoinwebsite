import React from 'react';
import SigninSection from '../components/Section/Signin';

const SigninPage = () => {
    return (
        <div>
            <SigninSection/>
        </div>
    );
};

export default SigninPage;