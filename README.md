# Gamecoin Website
![Design homepage of the Gamecoin Website](./src/assets/images/homepage.png)

## Description
This is a responsive gamecoin website project developed using react. You can click on sign in and you a redirected to a form (but not working if you valid).

## Technologies Used

React

React-Router-Dom

React-icons

React-scroll

Hooks

Styled-components

Axios

## Installation

git clone https://gitlab.com/Yutsu/reactgamecoinwebsite

npm install

npm start
